from django.test import TestCase
from django.test import Client

class ActivityTest(TestCase):

    def test_url(self):
        response = Client().get('/')
        self.assertEqual(200, response.status_code)

    def test_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')

    def test_web_page(self):
        response = Client().get('/')
        fields = response.content.decode('utf8')
        self.assertIn("ABOUT ME", fields)
        self.assertIn("Current Activities", fields)
        self.assertIn("Organizational Experience", fields)
        self.assertIn("Achievements", fields)