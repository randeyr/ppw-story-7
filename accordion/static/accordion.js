$(".toggle").click(function() {
    var content = $(this).parents(".accordion").find(".content");
    if (content.css("overflow") == "hidden") {
        content.css({"overflow": "auto", "height": 15 + "rem"});
    } else if (content.css("overflow") == "auto") {
        content.css({"overflow": "hidden", "height": 0});
    }
})

$(".up-button").click(function() {
    var content = $(this).parent().parent();
    content.prev().insertAfter(content);
})

$(".down-button").click(function() {
    var content = $(this).parent().parent();
    content.next().insertBefore(content);
})